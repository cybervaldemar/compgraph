#include "stdafx.h"
#include <C:/Users/michael/Documents/glut-3.7/include/GL/glut.h>
#include <math.h>

#pragma comment(lib, "C:/Users/michael/Documents/glut-3.7/lib/glut32.lib")

GLfloat xRotated, yRotated, zRotated;
GLdouble base = 1;
GLdouble height = 1.5;
GLint slices = 50;
GLint stacks = 50;


#define PI 3.1415926535898
#define Cos(th) cos(PI/180*(th))
#define Sin(th) sin(PI/180*(th))
/*  D degrees of rotation */
#define DEF_D 5

/*  Globals */
double dim = 7.0;
const char *windowName = "Lab 1";
int windowWidth = 500;
int windowHeight = 450;

/*  Various global state */
int toggleAxes = 0;   /* toggle axes on and off */
int toggleValues = 1; /* toggle values on and off */
int toggleMode = 0;   /* projection mode */
int th = 0;   /* azimuth of view angle */
int ph = 0;   /* elevation of view angle */
int fov = 55; /* field of view for perspective */
int asp = 1;  /* aspect ratio */
double LookAtX = 0.0;
double LookAtY = 0.0;
double LookAtZ = 0.0;

int objId = 0;


void project() {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(fov, asp, dim / 4, 4 * dim);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


void setEye(double x, double y, double z) {

	double Ex = -2 * dim*Sin(th)*Cos(ph);
	double Ey = +2 * dim        *Sin(ph);
	double Ez = +2 * dim*Cos(th)*Cos(ph);
	gluLookAt(Ex + x, Ey + y, Ez + z, 0, 0, 0, 0, Cos(ph), 0);

}

void drawAxes() {
	if (toggleAxes) {
		double len = 2.0;
		glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_LINES);
		glVertex3d(0, 0, 0);
		glVertex3d(len, 0, 0);
		glVertex3d(0, 0, 0);
		glVertex3d(0, len, 0);
		glVertex3d(0, 0, 0);
		glVertex3d(0, 0, len);
		glEnd();
		glRasterPos3d(len, 0, 0);
		glRasterPos3d(0, len, 0);
		glRasterPos3d(0, 0, len);
	}
}

void vertex(double th2, double ph2) {
	double x = Sin(th2)*Cos(ph2);
	double y = Cos(th2)*Cos(ph2);
	double z = Sin(ph2);
	glVertex3d(x, y, z);
}

void drawShape() {
	int th2, ph2, i, j, k;

	int len = 5;

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_LINES);
	glVertex3d(0, 0, 0);
	glVertex3d(len, 0, 0);
	glVertex3d(0, 0, 0);
	glVertex3d(0, len, 0);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 0, len);
	glEnd();

	if (objId == 1) {
		glTranslated(1.0, 1.0, 0);
		glutWireCone(1.0, 1.5, 16, 16);

		glTranslatef(0, 0, 1.5);
		glutWireSphere(0.5, 16, 16);
	} else if (objId == 2) {
		glPushMatrix();
		glRotated(-180, 0, 0, 1);
		glTranslated(1.0, 1.0, 0);
		glutWireCone(1.0, 1.5, 16, 16);

		glPopMatrix();
		//glRotated(180, 0, 0, 1);
		glTranslatef(1, 1, 1.5);
		glRotated(45, 1, 0, 0);
		glutWireSphere(0.5, 16, 16);
	} else if (objId == 3) {
		glTranslated(1.0, 0.0, 0.0);
		glutWireCube(1);

		glTranslated(0.5, 0.5, 0.5);
		glutWireSphere(0.5, 16, 16);
	}

}


void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glLoadIdentity();

	setEye(LookAtX, LookAtY, LookAtZ);

	drawAxes();

	drawShape();

	glFlush();
	glutSwapBuffers();
}


void reshape(int width, int height) {
	asp = (height>0) ? (double)width / height : 1;
	glViewport(0, 0, width, height);
	project();
}


void windowKey(unsigned char key, int x, int y) {

	 if (key == 32) {
		if (objId == 3) objId = 0;
		else objId++;
	}

	else if (key == '-' && key>1) fov--;
	else if (key == '+' && key<179) fov++;
	else if (key == 'a') dim += 0.1;
	else if (key == 'd' && dim>1) dim -= 0.1;

	project();
	glutPostRedisplay();
}


void windowSpecial(int key, int x, int y) {
	if (key == GLUT_KEY_RIGHT) th += 5;
	else if (key == GLUT_KEY_LEFT) th -= 5;
	else if (key == GLUT_KEY_UP) ph += 5;
	else if (key == GLUT_KEY_DOWN) ph -= 5;
	else if (key == 'p' || key == 'P') { LookAtX += 5.5; }
	else if (key == 'o' || key == 'O') LookAtY += 5.5;

	th %= 360;
	ph %= 360;

	project();
	glutPostRedisplay();
}

int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow(windowName);

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(windowKey);
	glutSpecialFunc(windowSpecial);

	glutMainLoop();
	return 0;
}